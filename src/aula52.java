public class aula52 { // média aritmética

    public static void main(String[] args) {

        int[] nums = new int[5];  // array de inteiros, variavel nums, nova instancia de inteiros
        nums[0] = 9; // posição 0 recebe valor 9
        nums[1] = 10; // posição 1 recebe valor 10
        nums[2] = 5; // posição 2 recebe valor 5
        nums[3] = 6; // posição 3 recebe valor 6
        nums[4] = 7; // posição 4 recebe valor 7


        int media = 0; // variavel inteira media valor 0
        for(int i = 0; i<nums.length; i++)  // for para looping, variavel i valor 0,  i menor que nums.length, cada ciclo add uma unidade
            media+= nums[i];  // media mais igual valor posição i

        float total = (float)media / nums.length;  // variavel float total, float ponto flutuante, valor de media dividido por nums array

        System.out.println("A média é: "+total); // imprime total
    }
}
